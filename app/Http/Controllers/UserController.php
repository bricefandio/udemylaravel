<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UdemyUser;

class UserController extends Controller
{
    public function index(){
        $users = UdemyUser::all();
        return view('welcome',[
            'users' => $users
        ]);
    }

    public function create(){
        return view('add-user');
    }

    public function store(Request $request){
        UdemyUser::create($request->all());
        return redirect('/');
    }

    public function edit($id){
        $user = UdemyUser::find($id);

        return view('edit-user',[
            'user' => $user
        ]);
    }

    public function delete($id){
        $user = UdemyUser::find($id);
        $user->delete();
        return back();
    }

    public function update(Request $request, $id) {
    $user = UdemyUser::where('id', $id)->first();

        $request->validate([
            'user_name'=> 'required|string',
            'email_address'=> 'required|email',
            'phone_number'=> 'required|string',
        ]);
        
        $user->user_name= $request->get('user_name');
        $user->email_address= $request->get('email_address');
        $user->phone_number= $request->get('phone_number');
        
        $user->save();
        return redirect('/');
    }

}
